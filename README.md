# MonoMiner #

Author: David Wu

Email: dwwu16@stanford.edu

MonoMiner is a tool to extract patients who are (1) diagnosed with a monogenic disease and (2) whose underlying gene causing their monogenic disease has been determined. It reports, for each patient whose notes are supplied, a diagnosis comprising of a OMIM disease ID and a OMIM gene ID if there is a molecularly confirmed monogenic disease diagnosis for that patient. It also outputs an evidence log file with the sentences and specific terms in the sentences that it finds in the patient notes to support a patient having either (1) a particular monogenic disease diagnosis or (2) a particular causative variant in a gene. For convenience, each annotated patient is reported with his/her corresponding OMIM disease ID and causative OMIM gene ID. Additionally, MonoMiner outputs an evidence file containing the disease and gene sentences, along with the specific disease synonyms and gene symbols found in these sentences, that it used to annotate the patient.

## Overview of MonoMiner ##

MonoMiner first uses natural language processing (NLP) to identify patients who likely have (1) a monogenic disease diagnosis and (2) a molecularly defined causative gene. Then, MonoMiner extracts the patient's clinical phenotypes using ClinPhen, and compares the patient's phenotypes to known phenotypes of that patient's annotated disease. If there is similiarity between the two sets of phenotypes, the patient is annotated with the originally discovered monogenic disease and causative gene. 

### MonoMiner Requirements ###

MonoMiner has been tested with Python 3.6

Optionally, create a virtual environment to run MonoMiner with Andaconda by running

```conda create -n monominer python=3.6```

and activate it by running

```conda activate monominer```

Then, install all requirements by running

```pip install -r requirements.txt```

Optionally, update ClinPhen's HPO data by running

```clinphen --update```

### Instructions to Run MonoMiner ###

#### Step 1 ####
First, run the NLP pipeline to extract potential patient monogenic diagnoses. To see the usage, run

```python monominer.py -h``` or ```python monominer.py --help```

   The usage is:
```
   usage: monominer.py [-h] [-d D] [-v]
                    notefile outfile logfile patientcol notecol timecol

   Extract monogenic diseases from patient free-text clinical notes.

   positional arguments:
      notefile    File containing clinical notes, in a character delimited format.
              Must have appropriate header columns as described by the patientcol, notecol, and timecol arguments. 
              Can contain more than one patient's clinical notes.
      outfile     File to output results to.
      logfile     File to output evidence log to.
      patientcol  Column name in notefile containing the patient identifier.
      notecol     Column name in notefile containing the clinical note.
      timecol     Column name in notefile containing the time of the note. Can be a dummy
              column if time information is not available.

   optional arguments:
      -h, --help  show this help message and exit
      -d D        Column delimiter. Default is "|"
      -v          Verbose: print status updates.
```
   The output file MonoMiner generates is a tsv file with the following columns:

       Column 1: Patient ID
       Column 2: OMIM Disease ID Assigned
       Column 3: OMIM Gene ID Assigned

   The log file MonoMiner generates is a tsv file with the following columns:

       Column 1: Patient ID
       Column 2: Whether this line contains disease or gene evidence
       Column 3: OMIM Disease ID or OMIM Gene ID found
       Column 4: Synonym in the notes for the disease/gene found
       Column 5: Full sentence containing the disease/gene synonym
       Column 6: Date and/or time of note

   If you have more than one patient, you can either run MonoMiner once on a file containing all patient notes, or run it multiple times on separate notefiles, each containing all the notes of one patient. Make sure that all the patient's notes are in one file. If you want to use the phenotype filtering pipeline, we recommend that you split a large notefile into many smaller files, each one with all the notes of a single patient. While MonoMiner can handle multiple patients in one notefile, ClinPhen requires a separate notefile per patient. This can also help run your patients through MonoMiner faster as you can parallelize your computation.

   Lastly, if you run MonoMiner on multiple notefiles separately, concatenate the output files into one large file of patient annotations for downstream analysis. For example, on Unix machines, this can be done with something like 

       ```cd ***DIRECTORY WITH ALL MONOMINER OUTPUT FILES***```
       ```cat * > ***FINAL MONOMINER OUTPUT FILE***```

#### Step 2 (Optional) ####
   To remove patients with multiple annotated monogenic diagnoses with the script ```uniq_diagnoses.py``` To see the usage, run

   ```python uniq_diagnoses.py -h``` or ```python uniq_diagnoses.py --help```

   While you certainly don't need to perform this step, we have found that the accuracy of MonoMiner on patients with more than one annotated diagnosis is lower. This is because it is difficult to distinguish between, for example the different types of monogenic diabetes. Therefore, we recommend this filtering step.

#### Step 3 ####
   Run ClinPhen on each patient output by MonoMiner to have a monogenic diagnosis. To see the usage, run

   ```clinphen -h```

   Make sure to save the output of ClinPhen to a file, that is named in the form ```{patient_id}.txt```, where ```patient_id``` is the same as first column of MonoMiner output file. All ClinPhen output files should be saved in the same directory for the next step.

#### Step 4 ####
   Run the Phenotype Similiarity Filter. For usage, run
   
   ```python phenotype_similarity_filter.py -h``` or ```python phenotype_similarity_filter.py --help```

   For the parameters ```num_phenotypes``` and ```phrank_cutoff```, we recommend 50 and 16.0, respectively. However, feel free to adjust these for your situation. A lower ```phrank_cutoff``` will increase the number of patients retrieved, but may include more false positives.

   The output of this file is the final list of MonoMiner diagnoses.

### MonoMiner Example ###

There is a sample patient notefile in the ```example``` subdirectory named ```example_notes.csv```. It has two patients in it, patient 0 and patient 1. To run the entire MonoMiner pipeline, run the following commands

#### Step 1 ####

```python src/monominer.py example/example_notes.csv example/monominer_output.txt example/monominer_log.txt Patient_ID Note Time```

Likely monogenic patients are stored in ```example/monominer_output.txt``` and the logfile is ```example/monominer_log.txt```

#### Step 2 ####

```python src/uniq_diagnoses.py example/monominer_output.txt example/monominer_output_uniq.txt```

Unique-ified output file is ```example/monominer_output_uniq.txt```

#### Step 3 ####

Since there are two patients with patient IDs 0 and 1, we need to run ClinPhen twice on a notefile containing each one of the patient's notes. There is a file ```0.csv``` and ```1.csv``` containing just the patient notes of each patient in the ```example``` folder.

Run

```clinphen example/0.csv > example/0.txt```

and 

```clinphen example/1.csv > example/1.txt```

to get patient 0's phenotypes in ```example/0.txt``` and patient 1's phenotypes in ```example/1.txt```

#### Step 4 ####

Lastly, run the phenotype filtering step with

```python src/phenotype_similarity_filter.py example/monominer_output.txt example/ example/monominer_file_output.txt```

to get the final MonoMiner output in the file ```example/monominer_file_output.txt```

## Acknowledgements

In processing the data from HPO, we used the packages obonet (https://github.com/dhimmel/obonet) and NetworkX (https://networkx.org/).


## MIT License

Copyright (c) 2021 David Wu and Gill Bejerano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
