gene_flags = [
    "de novo",
    "zygous",
    "zygote",
    "zygosity",
]

whole_word_gene_flags = [
    "gene",
    "genes",
    "genetics",
    "locus",
    "loci",
    "allele",
    "alleles",
    "novel",
    "deleterious",
    "pathogenic",
    "mutation",
    "mutations",
    "variant",
    "variants",
    "insertion",
    "insertions",
    "frameshift",
    "deletion",
    "deletions",
    "missense",
    "sequencing",
]

positive_flags = [
    "year-old",
    "year old",
    "month-old",
    "month old",
    "week-old",
    "week old",
    "day-old",
    "day old",
    "confirm",
    "positive",
]

negative_flags = [
    'family history',
    'cause',
    'to confirm',
    'study',
    'possib',
    'individual',
    'unknown',
    'uncertain',
    'could',
    'daughter',
]

whole_word_negative_flags = [
    'negative',
    'if',
    'no',
    'currently',
    'confirmatory',
    'carrier',
    'vus',
    'azygous',
    'neg',
]


def has_gene(sentence):
    sentence = sentence.lower()
    sentence_words = sentence.split(" ")
    for gf in gene_flags:
        if gf in sentence:
            return True
    for wwgf in whole_word_gene_flags:
        if wwgf in sentence_words:
            return True
    return False


def valid_sentence(sentence):
    sentence = sentence.lower()
    sentence_words = sentence.split(" ")
    if not has_gene(sentence):
        return False
    for nf in negative_flags:
        if nf in sentence:
            return False
    for wwnf in whole_word_negative_flags:
        if wwnf in sentence_words:
            return False
    for pf in positive_flags:
        if pf in sentence:
            return True
    return False


'''
Copyright (c) 2021 David Wu and Gill Bejerano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
