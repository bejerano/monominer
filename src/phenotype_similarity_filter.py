import argparse
import collections
import os
import pandas as pd
import sys
from phrank import Phrank
from phrank import utils as phrank_utils


MY_DIR = "/".join(os.path.realpath(__file__).split("/")[:-1])


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def main():
    parser = MyParser(description="Filter out MonoMiner annotated patients whose phenotypes do not resemble the known phenotypes for their MonoMiner-annotated disease.")
    parser.add_argument("monominer_outfile", type=str, help="Output file containing all patient diagnoses. Should be the output file produced by uniq_diagnoses.py or monominer.py.")
    parser.add_argument("phenotypes_dir", type=str, help="Directory storing the clinphen-extracted phenotypes for each patient.")
    parser.add_argument("output_file", type=str, help="File to output final MonoMiner patient diagnoses.")
    parser.add_argument("-num_phenotypes", default=50, type=int, help="Number of top clinphen phenotypes to use to calculate a patient's phrank score. Default is 50.")
    parser.add_argument("-phrank_cutoff", default=16.0, type=float, help="Phrank score cutoff used to filter out patients with a low phrank score. Patients with lower than this cutoff score will be discarded. Default is 16.0.")
    args = parser.parse_args()

    input_file = args.monominer_outfile
    phenotypes_dir = args.phenotypes_dir
    num_phenotypes = args.num_phenotypes
    phrank_score_cutoff = args.phrank_cutoff
    output_file = args.output_file

    DAG = MY_DIR + "/../data/phrank_data/hpodag.txt"
    DISEASE_TO_PHENO = MY_DIR + "/../data/phrank_data/disease_to_pheno.txt"
    DISEASE_TO_GENE = MY_DIR + "/../data/phrank_data/gene_to_disease_final.txt"

    p_hpo = Phrank(DAG, diseaseannotationsfile=DISEASE_TO_PHENO, diseasegenefile=DISEASE_TO_GENE)

    disease_to_phenotypes = collections.defaultdict(list)
    with open(DISEASE_TO_PHENO) as f:
        for line in f:
            tokens = line.strip().split('\t')
            hpo_id = tokens[0]
            omim_id = tokens[1]
            disease_to_phenotypes[omim_id].append(hpo_id)

    output = open(output_file, 'w+')
    with open(input_file) as f:
        for line in f:
            tokens = line.strip().split('\t')
            p_id = tokens[0]
            omim_id = tokens[1]
            omim_gene_id = tokens[2]
            patient_phenotypes_file = phenotypes_dir + p_id + '.txt'
            df = pd.read_csv(patient_phenotypes_file, sep='\t', header=0)
            patient_hpo_terms = list(df['HPO ID'])[:num_phenotypes]
            disease_hpo_terms = disease_to_phenotypes[omim_id]
            matchscore = p_hpo.compute_phenotype_match(patient_hpo_terms, disease_hpo_terms)
            if matchscore >= phrank_score_cutoff:
                output.write(line)
    output.close()


if __name__ == "__main__":
    main()


'''
Copyright (c) 2021 David Wu and Gill Bejerano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
