import argparse
import collections
import sys


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def main():
    parser = MyParser(description="Filter out MonoMiner annotated-patients with more than one monogenic disease diagnosis.")
    parser.add_argument("monominer_outfile", type=str, help="Output file from MonoMiner if it was run on a single notefile. Otherwise, if MonoMiner was run multiple times on seperate notefiles, provide the concatenated file of all MonoMiner output runs.")
    parser.add_argument("unique_outfile", type=str, help="File to output results to.")
    args = parser.parse_args()

    INPUT_FILE = args.monominer_outfile
    OUTPUT_FILE = args.unique_outfile

    patient_to_disease = collections.defaultdict(set)

    with open(INPUT_FILE) as f:
        for line in f:
            tokens = line.strip().split('\t')
            p_id = tokens[0]
            omim_id = tokens[1]
            omim_gene_id = tokens[2]
            omim_disease_id_omim_gene_id_pair = (omim_id, omim_gene_id)
            patient_to_disease[p_id].add(omim_disease_id_omim_gene_id_pair)

    output = open(OUTPUT_FILE, 'w+')
    for p_id in patient_to_disease:
        if len(patient_to_disease[p_id]) == 1:
            single_omim_pair = list(patient_to_disease[p_id])[0]
            omim_disease_id = single_omim_pair[0]
            omim_gene_id = single_omim_pair[1]
            output.write(f"{p_id}\t{omim_disease_id}\t{omim_gene_id}\n")
    output.close()


if __name__ == "__main__":
    main()


'''
Copyright (c) 2021 David Wu and Gill Bejerano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
