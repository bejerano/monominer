import os
import sys
from collections import defaultdict
import disease_extractor as de
import argparse

MY_DIR = "/".join(os.path.realpath(__file__).split("/")[:-1])

DISEASE_NAMES = MY_DIR + "/../data/maps/omim_disease_to_umls_name_fixed.txt"
GENE_SYMBOLS = MY_DIR + "/../data/maps/omim_disease_to_gene_symbol.txt"


def disease_search(word_to_positions, position_to_sentence, sentence_to_note, note_to_other_data, disease_to_name, sentence_id_to_sentence, string, patientCol, dateCol, checker=None, gene_map=None):
    disease_to_positions, disease_to_syns_to_positions = de.find_diseases(word_to_positions, position_to_sentence, disease_to_name)
    disease_to_other_data = defaultdict(list)
    valid_sentences = set()
    for disease in disease_to_positions.keys():
        for position in disease_to_positions[disease]:
            sentence = position_to_sentence[position]
            if checker and not checker[sentence]:
                continue
            valid_sentences.add(sentence)
            disease_to_other_data[disease].append(note_to_other_data[sentence_to_note[sentence]])
    patient_disease_to_logfile_details = defaultdict(list)
    if gene_map is not None:
        patient_disease_to_genes = defaultdict(set)
    for disease in disease_to_syns_to_positions:
        for syn in disease_to_syns_to_positions[disease]:
            for syn_position in disease_to_syns_to_positions[disease][syn]:
                syn_sentence = position_to_sentence[syn_position]
                if syn_sentence in valid_sentences:
                    syn_other_data = note_to_other_data[sentence_to_note[syn_sentence]]
                    patient_id = syn_other_data[patientCol]
                    note_date = syn_other_data[dateCol]
                    patient_disease_pair = (patient_id, disease)
                    if gene_map is None:
                        list_to_print = [patient_id, string, disease, syn, sentence_id_to_sentence[syn_sentence], note_date]
                        patient_disease_to_logfile_details[patient_disease_pair].append('\t'.join(list_to_print))
                    else:
                        disease_gene_syn_pair = (disease, syn)
                        causative_gene = gene_map[disease_gene_syn_pair]
                        patient_disease_to_genes[patient_disease_pair].add(causative_gene)
                        list_to_print = [patient_id, string, causative_gene, syn, sentence_id_to_sentence[syn_sentence], note_date]
                        patient_disease_to_logfile_details[patient_disease_pair].append('\t'.join(list_to_print))
    if gene_map is not None:
        return disease_to_other_data, patient_disease_to_logfile_details, patient_disease_to_genes
    else:
        return disease_to_other_data, patient_disease_to_logfile_details


def patient_to_dx(dx_to_other_data, patientCol):
    returnDict = defaultdict(set)
    for dx in dx_to_other_data.keys():
        for other_data in dx_to_other_data[dx]:
            patient = other_data[patientCol]
            returnDict[patient].add(dx)
    return returnDict


def cross_check(dx1, dx2):
    returnDict = {}
    for patient in dx1.keys():
        dx = dx1[patient] & dx2[patient]
        if dx:
            returnDict[patient] = dx
    return returnDict


def main(notefile, delimiter, patientCol, dateCol, noteCol, outfile, logfile, verbose=False):
    if verbose:
        print("Loading disease synonyms...")
    disease_to_name = de.load_syns_disease(DISEASE_NAMES)
    disease_to_gene, gene_map = de.load_syns_gene(GENE_SYMBOLS)
    if verbose:
        print("Loading clinical notes...")
    word_to_positions, word_to_positions_nocase, position_to_sentence, sentence_to_note, note_to_other_data, sentence_to_disease_name_check, sentence_to_diagnostic_gene_check, sentence_id_to_sentence = de.load_note_file_full(notefile, delimiter, noteCol)
    if verbose:
        print("Finding disease synonyms...")
    if verbose:
        print("  by disease name...")
    name_findings, patient_disease_to_logfile_details_disease = disease_search(word_to_positions_nocase, position_to_sentence, sentence_to_note, note_to_other_data, disease_to_name, sentence_id_to_sentence, 'disease', patientCol, dateCol, checker=sentence_to_disease_name_check)
    dx_by_name = patient_to_dx(name_findings, patientCol)
    if verbose:
        print("  by gene symbol...")
    gene_findings, patient_disease_to_logfile_details_gene, patient_disease_to_genes = disease_search(word_to_positions, position_to_sentence, sentence_to_note, note_to_other_data, disease_to_gene, sentence_id_to_sentence, 'gene', patientCol, dateCol, checker=sentence_to_diagnostic_gene_check, gene_map=gene_map)
    dx_by_gene = patient_to_dx(gene_findings, patientCol)
    dx = cross_check(dx_by_name, dx_by_gene)
    if verbose:
        print("Ouputting results...")
    if len(dx.keys()) > 0:
        output = open(outfile, "w+")
        #output.write("Patient ID\tOMIM Disease ID Assigned\tOMIM Gene ID Assigned\n")
        log = open(logfile, "w+")
        #log.write("Whether this line contains disease or gene evidence\tPatient ID\tOMIM Disease ID or OMIM Gene ID found\tSynonym in the notes for the disease/gene found\tFull sentence containing the disease/gene synonym\tDate and/or time of note\n")
        for patient in dx.keys():
            for diagnosis in dx[patient]:
                patient_disease_pair = (patient, diagnosis)
                for gene in patient_disease_to_genes[patient_disease_pair]:
                    output.write("\t".join([patient, diagnosis, gene]) + "\n")
                for line in patient_disease_to_logfile_details_disease[patient_disease_pair]:
                    log.write(line + "\n")
                for line in patient_disease_to_logfile_details_gene[patient_disease_pair]:
                    log.write(line + "\n")
        output.close()
        log.close()
    

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


if __name__ == "__main__":
    parser = MyParser(description="Extract monogenic diseases from patient free-text clinical notes.")
    parser.add_argument("notefile", type=str, help="File containing clinical notes, in a character delimited format. Must have appropriate header columns as described by the patientcol, notecol, and timecol arguments. Can contain more than one patient's clinical notes.")
    parser.add_argument("outfile", type=str, help="File to output results to.")
    parser.add_argument("logfile", type=str, help="File to output evidence log to.")
    parser.add_argument("patientcol", type=str, help="Column name in notefile containing the patient identifier.")
    parser.add_argument("notecol", type=str, help="Column name in notefile containing the clinical note.")
    parser.add_argument("timecol", type=str, help="Column name in notefile containing the time of the note. Can be a dummy column if time information is not available.")
    parser.add_argument("-d", type=str, default="|", help='Column delimiter. Default is "|"')
    parser.add_argument("-v", action="store_true", help="Verbose: print status updates.")
    args = parser.parse_args()
    main(args.notefile, args.d, args.patientcol, args.timecol, args.notecol, args.outfile, args.logfile, args.v)


'''
Copyright (c) 2021 David Wu and Gill Bejerano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
