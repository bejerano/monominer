from collections import defaultdict
import re
import valid_dx_sentences as vds
import valid_gene_sentences as vgs
import pandas as pd
import csv


prefixes = "(Mr|St|Mrs|Ms|Dr|Prof|Capt|Cpt|Lt|Mt)[.]"
punct = '''!"#$%&'()*+,/:;<=>?@[\]^_`{|}~'''


def split_into_sentences(text):
    text = re.sub(prefixes,"\\1*", text)
    text_2 = text
    for c in punct:
        text_2 = text_2.replace(c, "")
    return list(set(re.split("\. |    ", text) + re.split("\. |    ", text_2)))


def load_note_file_full(notefile, delimiter, noteCol):
    word_to_positions = defaultdict(set)
    word_to_positions_nocase = defaultdict(set)
    position_to_sentence = dict()
    sentence_to_note = dict()
    sentence_to_disease_name_check = dict()
    sentence_to_diagnostic_gene_check = dict()
    note_to_other_data = dict()
    sentence_id_to_sentence = dict()
    noteNum = -1
    sentenceNum = -1
    position = -1
    df = pd.read_csv(notefile, sep=delimiter, header=0)
    df = df.astype(str)
    for index, row in df.iterrows():
        noteNum += 1
        note = row[noteCol]
        other_data = row.drop(noteCol)
        note_to_other_data[noteNum] = other_data
        sentences = split_into_sentences(note)
        for sentence in sentences:
            sentenceNum += 1
            sentence_id_to_sentence[sentenceNum] = sentence
            sentence_to_disease_name_check[sentenceNum] = vds.valid_sentence(sentence)
            sentence_to_diagnostic_gene_check[sentenceNum] = vgs.valid_sentence(sentence)
            sentence_to_note[sentenceNum] = noteNum
            words = sentence.split(" ")
            for word in words:
                if not word:
                    continue
                position += 1
                position_to_sentence[position] = sentenceNum
                word_to_positions[word].add(position)
                word_to_positions_nocase[word.lower()].add(position)
    return word_to_positions, word_to_positions_nocase, position_to_sentence, sentence_to_note, note_to_other_data, sentence_to_disease_name_check, sentence_to_diagnostic_gene_check, sentence_id_to_sentence


def load_syns_disease(synfile):
    omim_to_syns = defaultdict(list)
    for line in open(synfile):
        lineData = line.strip().split("\t")
        omim = lineData[0]
        syn = lineData[1]
        syn = syn.lower()
        syn = syn.strip()
        omim_to_syns[omim].append(syn)
    return omim_to_syns


def load_syns_gene(synfile):
    omim_to_syns = defaultdict(list)
    gene_map = dict()
    for line in open(synfile):
        lineData = line.strip().split("\t")
        omim_disease_id = lineData[0]
        omim_gene_id = lineData[1]
        syn = lineData[2]
        syn = syn.strip()
        omim_to_syns[omim_disease_id].append(syn)
        disease_gene_syn_pair = (omim_disease_id, syn)
        if disease_gene_syn_pair not in gene_map or gene_map[disease_gene_syn_pair] == omim_gene_id:
            gene_map[disease_gene_syn_pair] = omim_gene_id
        else:
            raise ValueError('Gene Map File Not Correct!')
    return omim_to_syns, gene_map


# Searches for diseases such that all of the words in a given synonym must be in the same sentence, consecutive, and in order.
def find_diseases(word_to_positions, position_to_sentence, disease_to_syns):
    disease_to_positions = defaultdict(set)
    disease_to_syns_to_positions = defaultdict(lambda : defaultdict(set))
    for disease in disease_to_syns.keys():
        for syn in disease_to_syns[disease]:
            words = syn.split(" ")
            if len(words) < 1:
                continue
            curr_positions = set()
            for position in word_to_positions[words[0]]:
                if len(words) > 1:
                    currPos = position + 1
                    if currPos in position_to_sentence and position_to_sentence[currPos] == position_to_sentence[position]:
                        curr_positions.add(currPos)
                else:
                    curr_positions.add(position)
            for word in words[1:-1]:
                if len(curr_positions) == 0:
                    break
                next_positions = set()
                for position in word_to_positions[word] & curr_positions:
                    newPos = position + 1
                    if newPos in position_to_sentence and position_to_sentence[newPos] == position_to_sentence[position]:
                        next_positions.add(newPos)
                curr_positions = next_positions
            if len(words) > 1:
                curr_positions &= word_to_positions[words[-1]]
            disease_to_syns_to_positions[disease][syn] |= curr_positions
            disease_to_positions[disease] |= curr_positions
    return disease_to_positions, disease_to_syns_to_positions


'''
Copyright (c) 2021 David Wu and Gill Bejerano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
